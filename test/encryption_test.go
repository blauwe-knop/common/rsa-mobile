package rsa_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/common/rsa-mobile/rsa"
)

func TestEncryption(t *testing.T) {

	var publicKeyPem = `-----BEGIN PUBLIC KEY-----
MIICCgKCAgEAt2Aa+/XFJMmAVu7dG5GoOvms9ADziMRgtnGN3Rbejk23sxS4nyOI
ngwTtP1tp5pRH8pfOmYNh2YuqK5seYlVbPJOvjrsk+tPirEMGIVsMF6Qg4J44ubs
H00zAFAQ45Sjg6v2RczFIyU0N+tY20IE5+AHSxmydC+b3mvsBM2fYWOO4FbxGwov
+SLp2M+oNdjvD2SKn+fufVbSSqVvZtBWjfUEcsDNfrDy1Ue8G8+gcUw+VvDmW6eP
qFym2cod6DlqSOEsE1D8eUv+mPzKbnutp4n6/n4dnKQwByusH2+IQknLSEimadrD
OtpLVB0KmmScAD7BclPVlC+F2V9555UIIh026HBmRHD3316TidfQnrd4ZXhAnsiC
I259Dz+WPTq2wtYrjbCkbcyvgQ3Xztq9dfQZGbAx/2lKwTO3DBbvnaK9ZMIvulHr
X9fKwyg5wtXtYjfXok85Zeanzj2GTZL/mpf+xXMrPRwwhA40FL1VK2+hMwhLRh92
D3u+qGNr5pUHYGvAxnEuLppbSL7ehnkWiKou6IjQWKZARyHeuvZQYUie9wN9EzJY
9Ex/udwSTSFiOIgriB7zimg8aLnmeSqRhqhbyPsIvGzigJV3f+wXTiXmQsWUYadK
fhOihgwuMqqIkzxgqPmd75xqFcFf+avWCzKtW5Nc/UNsK9g+YAvRIT8CAwEAAQ==
-----END PUBLIC KEY-----`
	var privateKeyPem = `-----BEGIN RSA PRIVATE KEY-----
MIIJKAIBAAKCAgEAt2Aa+/XFJMmAVu7dG5GoOvms9ADziMRgtnGN3Rbejk23sxS4
nyOIngwTtP1tp5pRH8pfOmYNh2YuqK5seYlVbPJOvjrsk+tPirEMGIVsMF6Qg4J4
4ubsH00zAFAQ45Sjg6v2RczFIyU0N+tY20IE5+AHSxmydC+b3mvsBM2fYWOO4Fbx
Gwov+SLp2M+oNdjvD2SKn+fufVbSSqVvZtBWjfUEcsDNfrDy1Ue8G8+gcUw+VvDm
W6ePqFym2cod6DlqSOEsE1D8eUv+mPzKbnutp4n6/n4dnKQwByusH2+IQknLSEim
adrDOtpLVB0KmmScAD7BclPVlC+F2V9555UIIh026HBmRHD3316TidfQnrd4ZXhA
nsiCI259Dz+WPTq2wtYrjbCkbcyvgQ3Xztq9dfQZGbAx/2lKwTO3DBbvnaK9ZMIv
ulHrX9fKwyg5wtXtYjfXok85Zeanzj2GTZL/mpf+xXMrPRwwhA40FL1VK2+hMwhL
Rh92D3u+qGNr5pUHYGvAxnEuLppbSL7ehnkWiKou6IjQWKZARyHeuvZQYUie9wN9
EzJY9Ex/udwSTSFiOIgriB7zimg8aLnmeSqRhqhbyPsIvGzigJV3f+wXTiXmQsWU
YadKfhOihgwuMqqIkzxgqPmd75xqFcFf+avWCzKtW5Nc/UNsK9g+YAvRIT8CAwEA
AQKCAgArY6ce5SlvqeofJ8fTpSRsR/WfirYVL3o+0SGjJa0leMg1rHp+1TaXRv5G
vgx7Mu1tG0JrHAipeAkkSplKLK+05qSxKFogKfaZN4lIKBHQZB/HrlCSR9epFGgz
8737S4lhN4g/PdOLnFr9vEc7IiTtBLpVD9CE41r7RwgCnvDOZ3NAK/JC1qdBSPyS
G5iOnTT7rGuMqKFqsOdzWC/C4RsJ3ebejDZTeuUKiD2/SuKIzGSXx8qJ91zrlni4
YbWv5B38/qKiM3B59vxYEMCJYeRWFzT3kLnK/aKLn87LZWWVYcai5OXTeDrnqw1V
6sU+gP3UpQS625FWzePa6ld6722L1liQXflwQUGOd1/Szhp9V7gHi8m/5aA1BI5k
rCFvD2kRTKsZZ0zUROUSYi2xHUhJKVv9S7YvQj4OzbZUeMfY1MmmevnyQ7EvSp6v
3FNSGpXCIArpQuBhs8GyAEs3+qfDN7vkBKZNW/PMuatESaRZ5ozqhhgsTEGE1baa
VceifSjwrDEI+KsDRWPHjBrkrbToViddDlomRFtOE1+c647CkBwAHfTG04WT+uqI
1OlTDznlx7UCe3gs1HqN8YTqWp58DHGKrrbdx+4DWfyUmJGtMIvyTqxiifIi9puo
Ia/WDP5iVXT3PpCbS3M5n+A5CEujZaomiYVHlRUpYv3HI5yi6QKCAQEA267Ym9RG
1KALgj3LqvdGVhTyhHrB/1PwAR92oSWJXCB8ElOErJgWinl8ebkQwlDgsVmdak6K
8LkZOCDFwK6NPageaUlMhiovXRBeY6Gxe780NACCpXBQQJ0vqQqKUZwqfxktnfuy
+DhpyNLL973Ikx5SV/tXozyy5qRkVQ9c6UZF1G8VUi/a1ig8PR1sxgK2HhpdCZ3E
MumRBhR+91NFj+yjAbYkQrQ+Ht8Jh9h27/4czNst9HS7f+Y40SuKW+VD54jyEgfw
DLlQpv/TLmrJ4si0by2bVoC7Ni1ZIbg18o/Ahm8+84xi4yGzLcbSN3sryRfn+XA/
gT9aaUk0hZBKmwKCAQEA1bCxxYR/0dWUJ6SJKsOYkA1mxZff3eWq67/NcCG0mTLu
mLd2YRIcB7LaFMVcGbBPkSUrBbHhQY6NQk7WtBWNt928NXcoDYBTuojTn5yRS8l/
hYgoXeaolwuIC70MzbuhGnJmUHeZji7u6VgjoPsn2DBh85LVstqzDRBcHpLQWRJg
+13StGF2ej1eKSDjJRwTeYvEX7qI/DPjLrsGZ/TE+HXWLbVa7lDnwxrwjpCjyGnB
EpUKqzgYvXURYiMneQdPw2OijpN4QDQYRkt0ddtOZAPWgNE8UGnuChp7KUseVUaj
usqoHt32YNqNN3+3PItpzLWXMwu1tnmCeEyD58pMLQKCAQASm3GvaUCCm/e9lVxd
48nqWqXcAMXTyZlHjxGuPo6u5fV8W+Sd9dfa7MVVTg6UVuNhQjTqHzL3hsYTEfuO
AXrnIQlKY7H+ny4Z1NwZ1kVBNQXH7c8jEitJ/cZerAzhMrgKwegyPHKBQc37+5bZ
KhMGGwhgeWKH6glBLeVtqvp0q8YYYzxMFM+VWh0YFBj1gJ9KV3NP8DQBF/V3rV6/
tibrNODtsS7LE5c7aCrXfcc9Nqnb1CjFTunewHJJjUWP2RByWRAf5No5Sa0CKCMM
CHGHKvbVf+hrYEX7JcYp6/9txy1Idb3ARUDO+jjCBNgjaORhiQvV/eLzIJmY12GC
eqFHAoIBAA+8VTrwSOFQ0vogWaF2idOBySGfz3JtqSp3E0/Ai6YEZCGG0QbQ6JOA
jiKdbezOWO2dSQ/AS5AiSTCq0ZCtTaROhb+CKMblvkSsMrk8NE7aZbOVlTNk+uE0
ji4fG8RCnthtuC8Qv5QCzMEOJoGCPSrkVTI0i9wB0tGRdNcjhIgqnE2mWQ/DZZAW
2Mo6i79908lNi4ZpHBFGWOJmD1C0a5TISJ9RDYMjHg31++TjrcviTb9qjkCRfvDk
oAUUBaIZ8bu3qI7LOT2xGGCEyeyr25ft2Gvf/IsHYeoIjS07RN6OtxvYNI0hVzVG
osOFeh7RPVc/wASYRidLx1nIeKYm9XkCggEBAMaUmsdhUCMcCpvLpuAqR8Xz+MKT
t/33A2fiZeKQ80r+nAs/2TNwmrqE3lZyApB/16kpVbudHdr/XUX9qxEDkOrpdG85
8ORzPknvb1C3b+lqv3YqP8ECg5NBcnng+0bZFJRyabIaFJZt0Jpo/GrCBlJfaoio
MLVPtWv1rkTdDLIIKoq3nYKhvFy5nn3Lf+1i3Mw33X0Ptp1hude8JIJ8lbj8MfpO
60YyIYlhjcm7QxIMSAULgoMvKEgo9ZyM5fhtE6kU28oewPYyOq1FNKDKHFZ1lzcG
CzXH4uwq5/nnDoZiuIwFUKHd76N1n4Yw0eAuhGKKZ9Xx8UWc59/JAz/j654=
-----END RSA PRIVATE KEY-----`

	plaintextMessage := "Data to encrypt"
	messageEncryptedInDart := "gQuQopDbp1VDDiaE8h6TP/UYs+ed/TVB" +
		"DJi0KisuUJ8KzZUx3tPKSHxR4Q02j54arnKTOHBAKh85t3vsWaRY6U" +
		"cO/G06tRwWT5HHAJiCUQekzXX0Cb3bGp5BZbdLRn3CNYP5UJZF7TOX" +
		"k1D4g49xBVxnJjg30TiqXZ8sJFDSL0jzXm0spul8DVknq9yV1ijPKB" +
		"DdvcDTSWmAUII/RoguUSwwGezXpysS7njkB/oFAnOY/soqu92d5OJJ" +
		"JYpJTGP//T5nUQ7YBoA4IR1vzgTYDzHky9y3Ns+ybH+9s0sT9fZaHh" +
		"Ta9CXLa4Wxo4792tuqFcvKtWpcFs7FwSVejgrCIc5mnI9+WPdGclO/" +
		"V6s6MY7u6FTP0zZsdjsuwmKzRggbbx6kun4wGakgVP+uWbeCx3J4YN" +
		"S3nh9mbZoMXwZjtL9L4I5zbnXz5LfKCbzH7Xu+/9/TKVSTd3mnktob" +
		"9fc8n/e99kuseGlgjK0Xy7F+FLXXRThx47o/bzbLiC2danEmdQz0ss" +
		"POtxQDhdhogwvvsCkOlHRQNERYYPfprLjfpPSq5OBcl+vf8uXU0Epp" +
		"+ap/6LTwf80qTF9T7a8DKKc3bSn9SyrryFA4EBpClu17+lQz4zioYl" +
		"LJOMgElHPrWpUcCLPYGbwIqmiJ6pTx7B/5BI7O3792e9ZdEH9pVu9b85s="
	pssSignatureDart := "as2y3UhTg2JYkNRkwF9gLTMM7v3+N3kfm1VdV+" +
		"mdKyadMluREP6UlmEHUYuqMJbyZML2D16iMyXx9mOBjD1FhSGrH871" +
		"ecymRI5HpJpNLmtDTj3IZKHkV5zP5n+F89FduTBgdJkRwqUMTr2ybX" +
		"/WrnEVU3vmMnOumMae2SHDUJGL5jTdUMm+AGO/dWpSutG8yzgPreR0" +
		"ab+54Uo7cZXbJ/lsJ4YSD7EWOdDXG8dHtnq2Wd924+ZNXk1s5gBsaG" +
		"VVN/1qxc3J0o/fAnqDgnapPvOCjN/lMpzYdzGZ+GDQY+s+H/PDriSt" +
		"YwwXEOckFcPwyghS2anyM1dVBuVC+f3IQxsVUU6FT2UsGBPu0aSh1a" +
		"/FMQL2pPxPNoURKhRslgtXo10NW+fC28n1nvuoL777vV/IzR59zLOl" +
		"hCStrJfNtt3k5CpBnx9Zh3hXHMqD7y1kl+sG8i+mFi+wugvCSucGHy" +
		"OA/YJuESgCDX9O3/xm86YMnGjVZiMnIIWgpojcZxMaQda9TC7zSb5H" +
		"lkZUopXbdwdaTq1FlRDEjfl2gYWLbLDrtEoCKh9FWWCrvw89a00FWj" +
		"fGQXU7rSGmZ11Qcx8/RcMhmhucxF0OTo/D+ygt9PiUKk//mlvkx7rp" +
		"dWqzHbGIg4Q0L7k4nFS8glrGruMIS4UDIiK8xSNtzAH/OC2tv5k="

	longPlaintextMessage :=
		`Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Pellentesque aliquam sapien at congue lobortis. Fusce dolor nibh,
porta eu lectus ut, scelerisque sagittis tortor. Pellentesque
iaculis velit ut nisi vulputate congue. Vestibulum dui erat,
venenatis non lacus nec, sollicitudin ornare turpis. Suspendisse
potenti. Etiam dictum placerat tellus in sollicitudin. In hac
habitasse platea dictumst. Fusce maximus lacus ac congue ullamcorper.
Curabitur vulputate mi tortor, quis tristique nunc hendrerit non.
Nulla ultrices quis lectus eu lobortis. Fusce varius lacinia
lacus, vel scelerisque justo fermentum in. Quisque auctor
dignissim turpis sit amet egestas. In hac habitasse platea dictumst.

Vivamus at justo pulvinar, elementum diam ut, dapibus elit.
Nunc orci nisl, semper in justo eu, sagittis mollis leo.
Maecenas accumsan velit quis lacinia ultricies. Donec feugiat
interdum odio, sed sodales massa facilisis non. Integer massa
purus, sodales sit amet diam quis, luctus congue nunc. Aliquam
a fermentum tortor. Sed sed consequat ipsum, quis mollis felis.

Aliquam in nulla vel dolor hendrerit finibus ac semper justo.
Sed rhoncus dolor ut nisi varius, placerat suscipit lorem
accumsan. Nam orci sem, convallis id rutrum vel, faucibus nec
lorem. Etiam purus tellus, ultricies non enim at, placerat
tempor dolor. In sit amet dictum leo, at vehicula felis.
Phasellus eget turpis ut sapien lacinia bibendum. Maecenas
vulputate mattis fermentum. Duis et tortor tellus.

Nunc dapibus et purus eget facilisis. Aenean blandit leo nunc,
non cursus turpis scelerisque in. Integer pellentesque purus
nec nulla laoreet suscipit. Etiam sit amet dapibus arcu. Nulla
id eros nisi. Quisque dignissim nibh sed massa tempus, ornare
commodo neque venenatis. Sed quis auctor lacus. Sed eleifend
dui non nulla dapibus, ut imperdiet nibh dictum.

Vestibulum vel eros euismod, imperdiet nisi a, imperdiet orci.
Sed aliquam mi sed urna venenatis, sed feugiat libero luctus.
Aliquam non nunc nec dui finibus interdum at eget metus.
Suspendisse lobortis pretium purus, ut volutpat nisi elementum
ac. Integer leo augue, facilisis ac neque ac, vehicula facilisis
nisl. Morbi ullamcorper nulla vel nulla iaculis vehicula.
Mauris eget lacinia eros, id vestibulum massa. Fusce eget
blandit odio, vitae condimentum metus. Aliquam vitae nunc ex.
Curabitur dictum dui id diam dignissim, eu vulputate arcu
euismod. Cras ultrices tempus accumsan. Morbi laoreet, quam
quis luctus ultrices, nulla nulla sagittis mi, vitae pharetra
arcu tellus sit amet elit. Vivamus vestibulum ac mi sed accumsan
Mauris fermentum aliquam lorem, sed rutrum nulla suscipit nec.
Quisque porttitor tincidunt risus, nec posuere nibh aliquet
sollicitudin. Pellentesque habitant morbi tristique senectus
et netus et malesuada fames ac turpis egestas.

In dapibus nunc sit amet nunc feugiat consequat. Mauris cursus
metus id urna venenatis, eu consequat velit porta. In hac
habitasse platea dictumst. Quisque cursus, est id aliquet
cursus, est risus molestie risus, id iaculis velit velit ac
urna. Aliquam a mi at dolor pharetra sollicitudin. Sed id
imperdiet magna. Integer sagittis felis ex, sed posuere turpis
facilisis quis. Vivamus sed sollicitudin urna. Donec a mauri
quis erat varius placerat a eu nisl. Nam nulla enim,
pellentesque gravida purus quis, imperdiet consequat
lectus. Mauris pharetra mollis mollis. Nulla facilisi. `
	longMessageEncryptedInDart := "osfAl6cjJyl7bUCEIOilX1eGpPHrW8RLHwffwClRnKrHrSC61hb9qj9LZdiFgtkoK2lKq7aFME0s4Y5PR5v1FmP/U/fryFlIvkXZGjhgcWG5u0dEBeDiwqiw86vg2PsijREP9/E1+IYhG6jsB6GnlCIkL+ivDkuD6YCxJDC2JBpJfA++mS4K3PbpAvXdNjITiTBUdo0ZT24fE04WB4Up7UUymfsJDcBL1Byl+0IpWJzOWM81163a8Nm1QH3KPziyzDTISwu+wLAbD04S6v1c6mE3eQp/qVh0bXtYGCYEQ6yVlMYI+ngO5h+utJVYN3zuk3ui7KS6soJ4+Z3uHjr+lB3b5YwjcPYcGyPmdqpf8zX/Fihm1bFbB2nsCqWiqRoay51G4crPQs+6h5jRpC47ORJ5xobSeiNYIv1AnCfcySMbQmr5aepJ6wh00NRDbY0WMXFhJVA1FTHOEKuKywlcNTm5rSUiRoM+RNlg763LgXQbuUnkut2wKN7ize+bLXFy+vSdi1dFrQAWwKAoCcyBg5nKn4SVB0sBkZ/2tVgL10lJJgs77CQEOVZftxaR8HlAaK4GRjdsz+mw6TEokBUyMlBfO6kWeqTv44+yZ4fl/7RBBYrvWI4c3DgoD+I5DjGSIQ0oByIEhJdv9qcIGSGHU+f+jvbFm6RXQbo4he+wXRyBvMEmkYQPadpWQzZzCZqX03iOwjKwjXYnfLRQD2FKICCX6ag4vLVJB37a8KKrT8HbqISx6KeFUyi8wvGWwpwAIu7j5Wzta3/JZwBRyFNecFULo4vfm8Q+a6wPCZtQ/mNUJds2XjmETngQvIRVJQO3utzHMYI1l45BrSWpP9XeXJ439bmRHYP2NQ3RZNgKyEWrXm2t5fmNyuqB9hKkEFrSB2rHGzY3kQ3T0QkGJWDN33Z5ybi3T4NLScDddcJAY0Tslwd06SGLfnd6KIfz60ZDtI3Qd8Z0wj2mOSlCP66Ed6q6g4h+3j+YRoou/5rkKvPyP4ORKa++n6qwRPiHZIB35XrmiAuU4FJCVdQWHlppKGIw2gshwDsku9qT1Cdkh4i61qO53fOyrSg8J8gfqFdB2GLQ/RN24o/VdEauMMoWCn+NFI1AdSQIxFXpiaYfQEcI5UJPKApnOmwVlZx9tb3vpll4Duj4rbpnCdZx8BtTZPfmh4JH6RrgwayDmx6e7DQoNpk0ZaCwiRGGCa5GT0nIWymm3FqRojnrk8fXD6BU5tDePazlR8VWBrYZOoI5nUkgvYOIM6OfGzfLAhtpV1jIMqUalf6vG4eU7ePWsThsWc+wkJ0PRekxuj+XWaNKR50bqXx8XHjAdK/49ojXAqv02AGXo3mxne9Q/SIi22k7di+CTo+xRsU5soWtT9TohuUSDyj1mruCRIinlbQeVVRX8LsHH1wAzFfuF6Q4EyfwAIpsQbtP8C25XHD5DLP/0uQgGYZUbJniTMm1L7mfxPHTn/tQQuODnXEerfs2OpfoLby+dOX1/l6d4HIu4ZpLK5bos6l69p2LPcQXDfGso+JU/NHXVW7OTp+87RwMwEaAycP0sj8lywLjflXnkJ7TSBnPzm3ae4r8XINc4qvp6ODQqKLtWIzNx0zhyIIRrCAx4E2oTpBVji0aOUuGKXNFKg/2DlyZ7O9JTGBhfa4a+BofUVBgB/eFaNxdyG3aLMeecJZx0sw0qFXnHNQR7BPPRGKEtCOfFrhJcaOf+G+rsv4zS8WqDztEQCkvPyyt0JzTBkHoKQiqz9hstlhxpCUHVUy5Ud0kI3IMpf8ors8nooTTXYLQUg6U8hd4PNGBFX3ouD7t5AU6OcLFTZjaQOpZO/F/yLPsjoJePMPEiD0lRj7N2h4lYBKWGA8wdBAhrR4pKzmhXmQyWgqDzgFDhOhCW75xWBtkoFcebHKdiTiDMkMnfjN1nsdN9edX25xYDc62wrao8LtPncETY3VynVzwIy/k4o6thklB00SEDeBgA3eQi0zSSu5H5rxOjFoSPkn7T3i2R7jrhRtlONJ2JDJJP5Txfh8WvwzkY6l9ktoKKXAtDPpPvPGzqm2gKcDoOocdKVVZ5S36f4Sf97c2P8fkpnH+nf/l2R3lWHlvK01iDd53j5kkWiYyqZW6PwREX56IQ7LPcT7gz1AyiDampzoXseN+v/2Xeh2lJtBs8T+zFY8fkW2NYNAABgWIsKkrFD7gdABZv7Ax/yvqGHKVrZCrQHYgqeoHjbawtt1ku0uLgRq65Kl6xR6MwAg5GTAYY2HrPMJ7HcFMlXZF5lRenNltSG+VE1pA6kfvQx8KkZuSBvadCrLL3dz9nEb81i0gmgJhS1COx6cQTchspZnwk3Xey+ky2+5/p6tT8UNvhvOujdmf3KHSlC9NIkIVZ3rE/uEBsOhLSgme10ocR71otD/IrlPXoXe7CFZicCqHXAaE3MWSFLEf/TwFLL3SbQabLuLP86tZJJMC7YH+kKNwGU3So6OR8aBKkCHrK13o0JwmG+rDkvLeV3EQH2JvaUuUNjnUeMxCajyXx2LIAKJ2DbkVvJt3ASMjcgg3S8uLeWZZU4n6Q49HIVsjAZopnO6di9QSw3fpn8/DI+/ad8veIshmR6Yv6AE5MNQJt7TCY3lbN80V7I7Kg/r8mOM5Tbm3QTDVb7nusQVtTNyY30McXXI/KmEJS/XRC5rPYZCqqRa5iFcHayfN13XKKzYZ7MsgU3ppAq+vpLj6L6YsSK23JJesF3usH/YLFf2BFVIN4j7qmXrGXQm4VyncXefcEe8NgOvOZtOxKaOG9/ywYWs1+ZrzkToL+CHXih6fc9rV2x+jF5EXUfHnWFRZNIKVLjnVmJzVJyTiCHQ+m0SVSPRAAvWB8pmNXdZJC7tsoVVnqSw5eykZ7w7INICXCRXzYmfv7cOgm+bk6Kj5eVMQqf7DWBb6wsktDKZee26cmarSQ4S6s2HR4KUqwjdvrLABfnfdkc7SApJ/fUx2tZG71/FKq7j2QDNARA0V7+mr5yxXvG6dTjHaM/GpCwVHdAUVik/O1oFUuwM/M/kK74BFVi9lYmNir9kRgvaGFJovP7JoosjNxzxZHky+hLq7m25LNx4jDEcg7yxDByAL9f+aSPuqEcnkbGtN1sYhWcsGHHvjeL5Arcu+/4ny6V8NfgWX4Zxs1Vpm3e5LeqhpjRWOpjB5Kd+BA1JqvWMZKxvZziEVGHUL+rqQsJ9xuLDKARYLwIpOctGbWA/U57G4GObpvisTUl+JLBCbqfYZb4mWFvCCEe/nhQkCM6F33ikq57mSTkTvBMi5M6pB4a9UeZ6hv9cC741rJeqho0n1892U17eEk25QEhTu+vHvUjHJscQzbSVYsd1kINMmv23B2/Q78S1/IM8/UBdLygv2vXUvmIAoyAexVf5sBxV6oMoq5EYgtvYt3boo8lFDhxjDV57lIQcKj9wv5AZg5VqnLcMSc1DCMkWqSv08x1V9f263dlKvp8QMP8Sq+zWwP1T9ZSyhJzuXsfzYqORZaCnCQk3Wn291aLdX6gXUNsaAwfpcys6uIzg3ce94o+23wTU0O5oWHcaJ16QOAL+vrcAWzrO4zo9zNpW9/inQ4fyu9f8ab883U4OICL7cT2A0a9UsukzSFhg2tGLBbMf/7FBGzOYbJyO+4xC8AaDsWe5e9VONHBUCm2Mc6yJf/E64uCVP1g0kLEdHG/5cT7ptW8tWdAkXNyz9gT6skhG2gjqG2xURqzwVzir+Lw9iUT0JnCNb226g3/SoMqLj5Ii9HQGXlaoYKTKoroYEYDg/i0lHE6SuO7ebzv/7Y6Z/AZuWC7BBRpQ3DsW8wtF3alSw5CC4/P6vHD7mxg7zsMP/QzI+awLskJBlcCZIaWxJ9H/3JgYfC+Xqb/3b8jbWJFtaT0X8JgojX10FNdDjgMllE7oEGs7GF4M68TKt8nhRQ6mkPKpSnGuawGPdL11UH3KOcyFSuRWD6m8g/kZ9oUJ/q8uZ7t8sCrEQ1pHa/8NLXK/O8fwl5cfO0bnHFWUaUYg/FTntG81MWdle/REd5Gb7BWvAG5Li43pV6J7C2vOrKjHENWBzxL2bcJUoXGoO4yZai1OkTvrfJdlqSJfn1XGkhlW0ljAkVBUOFCHZlTQJrnjHdFTdrhpvRkETEpi/XmlBi6m5sVrCSHEKTo08m+vVVfHwswHO518lk2F/+joO/0kXGQhHDX8xyNS5Nw4upBh1h2JDCqkLDyyG4c9zBynkBsNNDC1XiZlL3GH1Anxp10mx7XCYoKi/Pd2m+aCTJpposeo/7Ov4cJhxjve8pAaRm3QSoYu0gfm2txLZ8jC5h15cC9tUZztUehNRzSJ/WSjxvrLui9tR6z+Ov757/8NssWORycGzFq82bA9mLraSzwyI8hWWMVIjLBsq0MVavDWnhVb2fOXWRu7RjME3PKC94SEX817u6gAcr6U2q4nMNkuxO0MXinUGTLMPF2lQTHBb4ITD+PecErnziTtrQGXikbwXMqUGTsyzel/dMjUfeCHUDWy3IDmX/KwEHMKLmvj2dYy5r3255VsIb9sGShSmNzDNTUtJP55SS9XXULZxvVhGMwu9olI2usD+r20328eJKXQ5RNl3j2+a+XHPTJKpIM2UOMHcuUjzOC2Q1RyuizxN8XeNpnuqXCVVWrX95KJY7WsLgkp8sc1R7S2zjmfcurQyL01ImE6+JStjSIQr9Et/ckuCYfaPrk1SeyslMEcErI7NlJ5jV8AN/veL4cMDvfNRrvFuM/CNPDLkN3XReNfEQMKRlHm7COlj4cyjBwZlQFk0MR2bGL4pqQR6BjMXloGADfRL3S5mKAGa81XniSvaKDr7tjtG2uxCu40LPf6ILrf93sFUnaxx+JzoUIvqBZHUEWH9tdZ3bTJkTwMpug8yAMGy6KXRVX31yiA5UWRQ6nbrllGSszurS0S7bpSfBTPi5azWO6bcbNaLWV7MTMOafpe+3auXBxOPao6TQDPnAzXkLP7skTy0rdf17vDLj0yMu7WDQIK9pPehniR8HrtXch2v07eKSJITh54VuxsWzfWQUKAfBnE/Fb70pwHY8zI2GPqHZQDVzc/v1nuCDPS0lU67P89NutKSwY+E0y3QJbMtB5U5ZLMpERrPvOufmyKNIbUE3Dl74eVclrZhLGLAq/63HEq5ubGujfMwruQie9CjTj7JvpNSC0PDsTPV+1k1t1qb+dgW4c44jBuoWf71O1Dmjp9A6/NZe9krwVpiV04Hx6/H6M97fEhVF4/YUPdWcqFzZMIr4Nai6GRw3r28aOQBD/6g4wfWTdO9OaT20c0jcANrc2ZYjTOtEa0sFoXk6xtDbnmzvQo6LiY1OONL5RBeYuQA3Ugggnxd1S69/D+qi8WXYHs4Ju07Bi8Wn9Or9mRJhmID5kMao6HXYxFvut4T88xjYHNYQTMDuJHvKxkG6ClI1KaksF9HeUDirDkkFFEVqvzO5oFnAuWBwg=="

	// messageEncryptedInGolang := "Qskq7rPYPjM6k6Kh+v8ak/ofb5gzi2HUbtUAH8xiVwfdQesekrMTsiS9sVKY68cQ5f9XB+06Ev18eF+DtF+jefEjmQjSkpDhNhrkbNQFAB0O/K6uGZgv+APi5+mDs7aAWalTu0y/NktbnIcskUctgCo3VIUclM3Itaxb/oWy9rv59SnrZA4yQDFamzHCJNKtp+1MFn8hKoQFRCDJ1OQy+BcDfd4GFP0YMA+bEamPdtkG6b0bSs6htqGtcyVTJvLdUqpLQbCi8GeCnlIqYRd72EDw4hDBkp4m1I8jzB5TRi2gWhjkuoSNqryIwXXcHD2ba32GI6RAVYd7AritWB+/fmgLLNDKYV+W+FMiPKugHpStoZIZWH/AS/838LHm2vbvXDWrxuH0roqKjiGut46VbwZtVJQPrXNB7bQJ1zaul6JLt39w/Qp2TVUj0A/mV6iTaot6Sl6Qtu4zX8FmSnSnXS7gyM9FFk0T9nh8bC1p+8QAkmQOljnAY9FfuX+1ztYpVaABssQGVVyEZwzPn3dbVkZlgFVmmXQznWPLSJWQIFXoNJ6GrtCQSN9AzLNVw5wlbGAAWCYGUfdSlxbbCtwHp9Zn/m4P/70EHmfIqCanHyWhQeFk11q+uEX7gngi7lboql4uBN6MYqGKm5qK9/Fz0MCAxprlPLojZ0Nx9EuB2ZM="
	// pssSignatureGolang := "YHX8s2sSlq6MG/OumQENMqqdOmO+u3vhP53P3WT7ltbWRnSIQ6FE8pAg0YTuccCQAB2qsRDTh+oEPdIe0qgYf5tHVTdhdi3JoT1hrznlNwLSoi0mbq0V7iK5hcPe+75FxUrCnHwBeVQbqNZke//IGhRDXJl0FxtP2GFtWrEW4wE6AeC5Ll5ECixc7DFyaCZZe0N8dpc/S5Xa5nz//ZOWkeMQ9GFS3NLlCfLs9Ma+/9TIi7CKhTGq04UAuT5CvfsKL5FHTSDvsobwYWdJlWUeVJn89zCBlLKmQ8wIcLj8XM0lyF4b+mbnZ5HxW85Jz4wQ1zc3MuHNYEj2fVFZRQGbcdspNQ1npcW963uRpgMQ710xs6anPh1TS1AA26rcJw+Hvd4nFksOymdgg1Fk3H8wTJsU2aBaRIDAbUgbZAkYyeLvHI2lgmLZfsFV1CdMJYe5dINpc12YBba2nJUVbMoaNfXkRRN+nTnHlSyReb4mPbFBzu/3vGIk5srOXP93NGfwAmOL53Bg7rUe57ud0bt0t2188RNDx0bDFoKshe3Avt4p9w2T6kkIUKU7Mp61Pkvl8T1qAPFl+0JJann8boxzctXe7ZiWe1WYX7kc6/ABGnexmF/X5x00Nl6VtPxxNh37BrDw+lBJWFX3ZOk7DlPopMKz/s8q58GvWcLcju0ugQI="

	t.Run("OAEP - encrypt and decrypt a message", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		encryptedMessage, err := instance.EncryptOAEP(plaintextMessage, "", "sha256", publicKeyPem)
		assert.Nil(t, err)

		decryptedMessage, err := instance.DecryptOAEP(encryptedMessage, "", "sha256", privateKeyPem)
		assert.Nil(t, err)

		assert.Equal(t, decryptedMessage, plaintextMessage)
	})

	t.Run("OAEP - decrypt a message encrypted in Dart", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		decryptedMessage, err := instance.DecryptOAEP(messageEncryptedInDart, "", "sha256", privateKeyPem)
		assert.Nil(t, err)

		assert.Equal(t, decryptedMessage, plaintextMessage)
	})

	t.Run("OAEP - encrypt and decrypt a long message", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		encryptedMessage, err := instance.EncryptOAEP(longPlaintextMessage, "", "sha256", publicKeyPem)
		assert.Nil(t, err)

		decryptedMessage, err := instance.DecryptOAEP(encryptedMessage, "", "sha256", privateKeyPem)
		assert.Nil(t, err)

		assert.Equal(t, decryptedMessage, longPlaintextMessage)
	})

	t.Run("OAEP - decrypt a long message encrypted in Dart", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		decryptedMessage, err := instance.DecryptOAEP(longMessageEncryptedInDart, "", "sha256", privateKeyPem)
		assert.Nil(t, err)

		assert.Equal(t, decryptedMessage, longPlaintextMessage)
	})

	t.Run("PSS - sign and verify", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		pssSignature, err := instance.SignPSS(plaintextMessage, "sha512", "equalsHash", privateKeyPem)
		assert.Nil(t, err)

		valid, err := instance.VerifyPSS(pssSignature, plaintextMessage, "sha512", "equalsHash", publicKeyPem)
		assert.Nil(t, err)
		assert.Equal(t, valid, true)
	})

	t.Run("PSS - verify a signature created in Dart", func(t *testing.T) {
		var instance = rsa.NewFastRSA()

		valid, err := instance.VerifyPSS(pssSignatureDart, plaintextMessage, "sha512", "equalsHash", publicKeyPem)
		assert.Nil(t, err)
		assert.Equal(t, valid, true)
	})

	// 	t.Run("verify pss with signature", func(t *testing.T) {

	// 		var publicKeyPem = `-----BEGIN PUBLIC KEY-----
	// MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQByxUwU4KV0gCQJroR/fuVm
	// b4tTBWBHqrE6KwK4W+s3qc1RvnBpKvfjK9rFBGRZ4q9Hu6p0M1O2BT1F08tC3rXP
	// Q1lKSo5ul6t8pwP4M5IOxMiRqh2z5z/5btuiSrKFaS/LV2fOe2XDZzIoXzHNbCmI
	// 9ArG7gKmbY9ZS3QukgsJMv0sq3SF+tLHylFbE6nLaYW/Z8xUVjp/iGt1ekNWXTn2
	// 8BTcwwAfzn2lyWeAMTdon4GIWF6kOFYyMn1P3STFRvRVA816SeHFcWxJt1wqArg4
	// JZ8YzKh6R3hDyvxxLj+p5CgoHNqoIQZsdQdunc9igN/kxfYWj/tbTFmPwKUyIMHF
	// AgMBAAE=
	// -----END PUBLIC KEY-----
	// `
	// 		pub, err := NewPublicKeyFromPem(publicKeyPem)
	// 		assert.Nil(t, err)

	// 		data := "Data to sign"

	// 		encodedSignature := "XL4oBzPFBYKZ5pu2Y05SY6ZMZX6nwCK5PAwftNFXuOp8J0gdUC0L4av/9Yi2WyqYAYrP3PjuP8EvAeWzOvZs+JtX8X6SrJtily8oIF/U9SishirDBYP2RUm4e2XN5iTp88lzpVeg6pAyH7dj4cGVRYQIRPJqlAzrw6cCPY/76sAZ32CAJyEbWBcx4Chs8HpDiYf7/y+hgnQyN9GoOuiS6CnnEiPJB8XyKMTHXevCVQlr8F9scb5hd3GJJ1PEkBeqtLw8ixWZpGn6INWYzEADUoMt2wP2/Ogn1bL51bkGncwwzErAoVJlu0vE6I/bbscyhITf/AAu2iJrjONzdVDqsA=="

	// 		err = VerifyPSS(encodedSignature, data, pub)
	// 		assert.Nil(t, err)
	// 	})

	// 	t.Run("encrypt and decrypt aes", func(t *testing.T) {
	// 		key := []byte("01234567890123456789012345678901")
	// 		message := "Lorem ipsum dolor sit amet"

	// 		encrypted, err := EncryptAesMessage(key, message)
	// 		println("ENCRYPTED", encrypted)
	// 		assert.Nil(t, err)

	// 		decrypted, err := DecryptAesMessage(key, encrypted)
	// 		assert.Nil(t, err)
	// 		assert.Equal(t, message, decrypted)
	// 	})

	// 	t.Run("decrypt aes with ciphertext", func(t *testing.T) {
	// 		key := []byte("01234567890123456789012345678901")
	// 		message := "Lorem ipsum dolor sit amet"

	// 		// encrypted := "z3LVVj7Y4jMSB1HPgBAZQOvtJPdCUauw3XLzThYjsGY="
	// 		encrypted := "nYmHSRWB4YCf0Po6hxB9B/vbvwlXfaCTtkp7o2B55fwEn4cvosmaHM9K"

	// 		decrypted, err := DecryptAesMessage(key, encrypted)
	// 		assert.Nil(t, err)
	// 		assert.Equal(t, message, decrypted)
	// 	})
}

// func DecryptOAEP(encodedData string, pri *rsa.PrivateKey) (string, error) {
// 	hash := sha1.New()
// 	random := rand.Reader

// 	decodedData, err := base64.URLEncoding.DecodeString(encodedData)
// 	if err != nil {
// 		return "", fmt.Errorf("failed decode string: %v", err)
// 	}

// 	decryptedData, err := rsa.DecryptOAEP(hash, random, pri, decodedData, nil)
// 	if err != nil {
// 		return "", fmt.Errorf("failed decrypt: %v", err)
// 	}
// 	return string(decryptedData), nil
// }

// func EncryptOAEP(data string, pub *rsa.PublicKey) (string, error) {
// 	hash := sha1.New()
// 	random := rand.Reader
// 	msg := []byte(data)

// 	encryptedData, err := rsa.EncryptOAEP(hash, random, pub, msg, nil)
// 	if err != nil {
// 		return "", fmt.Errorf("failed encrypt: %v", err)
// 	}

// 	encodedData := base64.URLEncoding.EncodeToString(encryptedData)
// 	return encodedData, nil
// }

// func VerifyPSS(encodedSignature string, data string, pub *rsa.PublicKey) error {
// 	signatureBytes, err := base64.StdEncoding.DecodeString(encodedSignature)
// 	if err != nil {
// 		return fmt.Errorf("failed decoding signature: %v", err)
// 	}

// 	var opts rsa.PSSOptions
// 	opts.SaltLength = rsa.PSSSaltLengthAuto

// 	newhash := crypto.SHA256
// 	pssh := newhash.New()
// 	pssh.Write([]byte(data))
// 	hashed := pssh.Sum(nil)

// 	err = rsa.VerifyPSS(pub, newhash, hashed, signatureBytes, &opts)
// 	return err
// }

// func SignPSS(data string, pri *rsa.PrivateKey) (string, error) {
// 	var opts rsa.PSSOptions
// 	opts.SaltLength = rsa.PSSSaltLengthAuto

// 	newhash := crypto.SHA256
// 	pssh := newhash.New()
// 	pssh.Write([]byte(data))
// 	hashed := pssh.Sum(nil)

// 	signature, err := rsa.SignPSS(rand.Reader, pri, newhash, hashed, &opts)
// 	if err != nil {
// 		return "", fmt.Errorf("failed to sign message: %v", err)
// 	}

// 	encodedSignature := base64.StdEncoding.EncodeToString(signature)
// 	return encodedSignature, nil
// }

// func NewPublicKeyFromPem(publicKeyPem string) (*rsa.PublicKey, error) {
// 	pubKeyBlock, _ := pem.Decode([]byte(publicKeyPem))
// 	var pub *rsa.PublicKey
// 	pubInterface, err := x509.ParsePKIXPublicKey(pubKeyBlock.Bytes)
// 	if err != nil {
// 		return nil, fmt.Errorf("Load public key error: %v", err)
// 	}

// 	pub = pubInterface.(*rsa.PublicKey)

// 	return pub, nil
// }

// func NewPrivateKeyFromPem(privateKeyPem string) (*rsa.PrivateKey, error) {
// 	privateKeyBlock, _ := pem.Decode([]byte(privateKeyPem))
// 	return x509.ParsePKCS1PrivateKey(privateKeyBlock.Bytes)
// }

// func EncryptAesMessage(key []byte, message string) (string, error) {
// 	messageBytes := []byte(message)
// 	block, err := aes.NewCipher(key)
// 	if err != nil {
// 		return "", fmt.Errorf("could not create new cipher: %v", err)
// 	}

// 	ciphertext := make([]byte, aes.BlockSize+len(messageBytes))

// 	iv := ciphertext[:aes.BlockSize]

// 	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
// 		return "", fmt.Errorf("could not encrypt: %v", err)
// 	}

// 	stream := cipher.NewCTR(block, iv)

// 	stream.XORKeyStream(ciphertext[aes.BlockSize:], messageBytes)

// 	return base64.StdEncoding.EncodeToString(ciphertext), nil
// }

// func DecryptAesMessage(key []byte, cipherData string) (string, error) {
// 	cipherText, err := base64.StdEncoding.DecodeString(cipherData)
// 	if err != nil {
// 		return "", fmt.Errorf("could not base64 decode: %v", err)
// 	}

// 	block, err := aes.NewCipher(key)
// 	if err != nil {
// 		return "", fmt.Errorf("could not create new cipher: %v", err)
// 	}

// 	if err != nil {
// 		return "", fmt.Errorf("could not strip pkcs7: %v", err)
// 	}

// 	iv := cipherText[:aes.BlockSize]

// 	message := make([]byte, len(cipherText[aes.BlockSize:]))
// 	stream := cipher.NewCTR(block, iv)
// 	stream.XORKeyStream(message, cipherText[aes.BlockSize:])

// 	return string(message), nil
// }
