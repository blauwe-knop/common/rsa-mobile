package rsa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFastRSA_VerifyPKCS1v15(t *testing.T) {

	instance := NewFastRSA()

	inputMessage := "hola"
	output, err := instance.VerifyPKCS1v15(signed, inputMessage, "sha512", publicKey)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("output:", output)
}

func TestFastRSA_VerifyPKCS1v15WrongKey(t *testing.T) {

	instance := NewFastRSA()

	inputMessage := "hola"
	_, err := instance.VerifyPKCS1v15(signed, inputMessage, "sha512", "nop")
	assert.Error(t, err)

}
