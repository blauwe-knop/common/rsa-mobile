package rsa

import (
	"testing"
)

func TestFastRSA_VerifyPSS(t *testing.T) {

	instance := NewFastRSA()

	inputMessage := "hola"
	output, err := instance.VerifyPSS(signedPSS, inputMessage, "sha512", "equalsHash", publicKey)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("output:", output)
}
